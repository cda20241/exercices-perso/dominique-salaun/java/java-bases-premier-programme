public class Main {

    /**
     * Le point d'entrée de l'application.
     */
    public static void main(String[] args) {
        // Message d'accueil
        String message = "\nBonjour";
        String message1 = "=======\n";

        // Affiche le message d'accueil suivi d'une ligne de séparation
        System.out.println(message);
        System.out.println(message1);

        // Appelle la méthode 'premier' pour calculer le nombre de nombres premiers
        NombrePremier.premier();
    }
}