import java.util.Scanner;

public class NombrePremier {


    public static void premier() {
        /**
         * Cette méthode calcule le nombre de nombres premiers dans la limite spécifiée.
         */
        Scanner scanner = new Scanner(System.in); // Crée un Scanner pour lire l'entrée de l'utilisateur

        System.out.print("Entrez un nombre : ");
        int limite = scanner.nextInt(); // Lit un entier à partir de l'entrée utilisateur

        int compteur = 0;

        for (int nombre = 2; nombre <= limite; nombre++) {
            boolean estPremier = true;
            // Vérifie si le nombre est premier en le divisant par tous les nombres de 2 à sa racine carrée.
            for (int diviseur = 2; diviseur <= Math.sqrt(nombre); diviseur++) {
                if (nombre % diviseur == 0) {
                    estPremier = false;
                    break; // Sort de la boucle si le nombre n'est pas premier.
                }
            }
            if (estPremier) {
                compteur++;
            }
        }

        System.out.println("Le nombre de nombres premiers entre 1 et " + limite + " est : " + compteur);

        // Ferme le Scanner
        scanner.close();
    }

    public static void main(String[] args) {
        premier(); // Appelle la méthode premier() depuis le main()
    }
}
